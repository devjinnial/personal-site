import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import Challenges from "@/views/Challenges";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },{
    path: '/challenges',
    name: 'UI Challenges',
    component: Challenges
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
